#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <getopt.h>
#include <syslog.h>
#include <signal.h>

#define VERSION "1.0.0"

double time_difference(struct timespec a, struct timespec b) {
  long nanoseconds =
    (b.tv_nsec - a.tv_nsec) +
    ((b.tv_sec - a.tv_sec) * 1000000000);
  return nanoseconds * 0.000000001;
}

int time_comparison(const double *a, const double *b) {
  if (*a < *b) { return -1; }
  if (*a > *b) { return 1; }
  return 0;
}


typedef void (*logging_function) (void *, const char * format, ...);

typedef struct logging_arrangement {
  logging_function log;
  void *info_log_args;
  void *warning_log_args;
  void *panic_log_args;
} logging_arrangement;


/* It's easier to have these as globals so that the signal handler doesn't
   need to pass handles to them. */

static struct timespec *actual_wakeup_times;
static struct timespec starting_time;
static int number_of_samples_collected;
static struct logging_arrangement *logger;
static int interrupted_by_signal = 0;
static int need_to_exit = 0;


void get_timings(struct timespec *tp,
		 struct timespec *starting_time,
		 int* counter,
		 const int reporting_count) {
  int sleep_success;
  struct timespec one_second;
  one_second.tv_sec = (time_t) 1;
  one_second.tv_nsec = 0;
  if (clock_gettime(CLOCK_REALTIME, starting_time) == -1) {
    perror("Could not call clock_gettime(): ");
    exit(1);
  }
  for (*counter=0; *counter<reporting_count; (*counter)++) {
    sleep_success = clock_nanosleep(CLOCK_REALTIME, 0, &one_second, NULL);
    switch (sleep_success) {
    case 0:
      break;
    case EINTR:
      return;
    default:
      fprintf(stderr,"I had a bizarre return result from clock_nanosleep: %d. Go and find out what that might mean.\n", sleep_success);
      exit(1);
    }
    
    if (clock_gettime(CLOCK_REALTIME, tp + (*counter)) == -1) {
      perror("Could not call clock_gettime(): ");
      exit(1);
    }
  }
}

void report_cpu_delays(void) {
  int i;
  double *intervals;
  double max_interval = 0.0;
  struct timespec time_of_worst_interval;
  double sum_interval = 0.0;
  double sum_squared_interval = 0.0;
  double mean_interval = 0.0;
  double interval_stddev = 0.0;
  int mild_wtf_events = 0;
  int horror_events = 0;

  if (number_of_samples_collected == 0) {
    return;
  }
  intervals = calloc(number_of_samples_collected, sizeof(double));

  /* Calculate statistics */
  for(i=0; i<number_of_samples_collected; i++) {
    if (i==0) {
      intervals[0] = time_difference(starting_time, actual_wakeup_times[0]);
    } else {
      intervals[i] = time_difference(actual_wakeup_times[i-1], actual_wakeup_times[i]);
    }
    if (intervals[i] > max_interval) {
      max_interval = intervals[i];
      time_of_worst_interval = actual_wakeup_times[i];
    }
    if (intervals[i] > 1.5) {
      mild_wtf_events += 1;
    }
    if (intervals[i] > 11.0) {
      horror_events += 1;
    }
    
    sum_interval += intervals[i];
    sum_squared_interval += intervals[i] * intervals[i];
  }

  /* Report statistics */
  logger->log(logger->info_log_args, "Reporting for %d samples collected in the period between:\n", number_of_samples_collected);
  logger->log(logger->info_log_args, " - %s",ctime(&starting_time.tv_sec));
  logger->log(logger->info_log_args, " - %s",ctime(&(actual_wakeup_times[number_of_samples_collected-1].tv_sec)));

  qsort(intervals, number_of_samples_collected, sizeof(double),
	(__compar_fn_t) time_comparison);
    
  mean_interval = sum_interval / number_of_samples_collected;
  interval_stddev = (sum_squared_interval / number_of_samples_collected) - (mean_interval * mean_interval);
  
  logger->log(logger->info_log_args,"Worst scheduling delay: %.6f seconds -- %s",
	  max_interval - 1.0,
	  ctime(&time_of_worst_interval.tv_sec)
	 );
	 
	 
  logger->log(logger->info_log_args, "Mean scheduling delay: %.6f seconds\n", mean_interval - 1.0);
  logger->log(logger->info_log_args, "Standard deviation on delay: %.2f\n", interval_stddev);
  logger->log(logger->info_log_args, "25%% of the time, the delay was less than %.6f seconds\n",
      intervals[number_of_samples_collected/4] - 1.0);
  logger->log(logger->info_log_args, "Median scheduling delay was %.6f seconds\n",
      intervals[number_of_samples_collected/2] - 1.0);
  logger->log(logger->info_log_args, "25%% of the time, the delay was greater than %.6f seconds\n",
      intervals[3*number_of_samples_collected/4] - 1.0);
  if (mild_wtf_events > 0) {
    logger->log(logger->warning_log_args, "Times where we received no CPU allocation for half a second (or longer): %d\n", mild_wtf_events);
  }

  if (horror_events > 0) {
    logger->log(logger->panic_log_args, "Times where we received no CPU allocation for ten seconds: %d\n", mild_wtf_events);
  }  
  
}

void handle_signal (int signal_id) {
  interrupted_by_signal = 1;
  switch (signal_id) {
  case SIGHUP:
  case SIGINT:
  case SIGQUIT:
  case SIGTERM:
  case SIGPWR:
    need_to_exit = 1;
  }
}


void collect_and_report(const int reporting_count) {
  struct sigaction handler;
  handler.sa_handler = handle_signal;
  sigemptyset(&(handler.sa_mask));
  handler.sa_flags = SA_NODEFER;
  sigaction(SIGHUP, &handler, NULL);
  sigaction(SIGINT, &handler, NULL);
  sigaction(SIGQUIT, &handler, NULL);
  sigaction(SIGTERM, &handler, NULL);
  sigaction(SIGUSR1, &handler, NULL);
  sigaction(SIGUSR2, &handler, NULL);
  sigaction(SIGPWR, &handler, NULL);
  interrupted_by_signal = 0;
  actual_wakeup_times = calloc(reporting_count, sizeof(struct timespec));
  get_timings(actual_wakeup_times,
	      &starting_time,
	      &number_of_samples_collected,
	      reporting_count);
  report_cpu_delays();
}




static struct option long_options[] = {
  {"count",   required_argument, 0,  'c' },
  {"logfile", required_argument, 0,  'l' },
  {"syslog",  no_argument,       0,  's' },
  {"help",    no_argument,       0,  'h' },
  {"version", no_argument,       0,  'v' },
  {0,         0,                 0,  0   }
};

#define DEFAULT_REPORTING_COUNT 3600

int main (int argc, char* argv[]) {
  int reporting_count = DEFAULT_REPORTING_COUNT;
  logging_arrangement file_stream = { (logging_function) fprintf, stdout, stdout, stdout };
  logging_arrangement write_to_syslog = {
    (logging_function) syslog, (void*) LOG_NOTICE, (void*) LOG_WARNING, (void*) LOG_ERR
  };
  FILE *output_stream = stdout;
  int use_syslog = 0;
  int c;
  int option_index = 0;
  while (1) {
    c = getopt_long(argc, argv, "c:l:shv", long_options, &option_index);
    switch (c) {
    case '?':
      exit(2);
    case 'c':
      reporting_count = strtol(optarg, NULL, 10);
      if (reporting_count < 1) {
	fprintf(stderr,"Argument to --count should be a positive number of seconds\n");
	exit(1);
      }
      continue;
    case 'l':
      output_stream = fopen(optarg, "a");
      if (output_stream == NULL) {
	fprintf(stderr, "%s", optarg);
	perror(" could not be opened");
	exit(1);
      }
      file_stream.info_log_args = output_stream;
      file_stream.warning_log_args = output_stream;
      file_stream.panic_log_args = output_stream;
      continue;
    case 's':
      use_syslog = 1;
      continue;
    case 'v':
      printf("%s version %s\n", __FILE__, VERSION);
      exit(0);
    case 'h':
      printf("  %s [--logfile LOGFILE] [--syslog] [--count COUNT]\n", __FILE__);
      printf("  %s --help\n", __FILE__);
      printf("\n");
      printf("This program was written because a customer of mine had \n");
      printf("some problems on a VMware cluster where virtual machines\n");
      printf("weren't getting scheduled very often. Sometimes minutes\n");
      printf("could pass before a virtual machine got scheduled.\n\n");
      printf("This program tries to measure that. It tries to sleep for\n");
      printf("precisely 1 second, but it carefully measures how long it\n");
      printf("was before it got more CPU time.\n\n");
      printf("You can get it to dump statistics at any time by sending it a USR1 or USR2 signal\n");
      printf("  --logfile LOGFILE -- write statistics to LOGFILE instead of stdout\n");
      printf("  --count COUNT     -- dump statistics every COUNT seconds instead of the default of %d\n", DEFAULT_REPORTING_COUNT);
      printf("  --syslog          -- write stats to syslog as local3.* (overrides --logfile)\n");
      exit(0);
    case 0:
	printf("Argument was %s\n", long_options[option_index].name);
	if (optarg) {
	  printf("And its argument was %s\n", optarg);
	  break;
	}
    case -1:
      goto OPTION_LOOP_EXIT;
    }
  }
 OPTION_LOOP_EXIT:

  if (use_syslog) {
    openlog(NULL, LOG_PID, LOG_LOCAL3);
    logger = &write_to_syslog;
  } else {
    logger = &file_stream;
  }

  while(!need_to_exit) {
    if (use_syslog) {
      collect_and_report(reporting_count);
    } else {
      collect_and_report(reporting_count);
      fprintf(output_stream, "--------------------------------------------------\n");
      fflush(NULL);
    }
  }
}
