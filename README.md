am-i-scheduled
==============

Do you want to know how much contention there is for CPU in your virtualised environment?
e.g. is VMware really scheduling your virtual machine when you need it? Have you ended up 
with an EC2 instance that is being shared with some other CPU-heavy jobs?

*am-i-scheduled* sleeps for exactly one second, checks the time and then goes back to sleep
again. Every hour (configurable with the `--count` option), it dumps a report on latency
and scheduling to stdout. Or, with the `--logfile` option, to another file, or with 
`--syslog`, it can write to syslog.

This is the kind of report it generates.

	Reporting for 3 samples collected in the period between:
	  - Thu Jul  6 14:57:32 2017
	  - Thu Jul  6 15:57:59 2017
	 Worst scheduling delay: 11.0209001 seconds -- Thu Jul  6 14:57:35 2017
	 Mean scheduling delay: 0.006195 seconds
	 Standard deviation on delay: 0.0003
	 25% of the time, the delay was less than 0.000171 seconds
	 Median scheduling delay was 0.002060 seconds
	 25% of the time, the delay was greater than 0.002090 seconds
	 Times where we received no CPU allocation for half a second (or longer): 3
