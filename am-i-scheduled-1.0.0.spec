Summary: A program that reports on how much scheduling delay there is in a virtualised environment
Name: am-i-scheduled
Version: 1.0.0
Release: 1
License: GPL
Group: Monitoring
Source: https://bitbucket.org/solresol/am-i-scheduled/downloads/am-i-scheduled-1.0.0.src.tar.gz
BuildRequires: systemd
%{?systemd_requires}

%description
Do you want to know how much contention there is for CPU in your
virtualised environment?  e.g. is VMware really scheduling your
virtual machine when you need it? Have you ended up with an EC2
instance that is being shared with some other CPU-heavy jobs?

am-i-scheduled sleeps for exactly one second, checks the time and then
goes back to sleep again. It dumps a report on scheduling latency. If
your VMware environment is very busy (CPU Ready is nowhere near zero)
or you have some unfriendly neighbours in your AWS hosting, you'll see
scheduling latency and am-i-scheduled will report on it.

%prep
%setup -q
%build
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/sbin
mkdir -p $RPM_BUILD_ROOT/lib/systemd/system
mkdir -p $RPM_BUILD_ROOT/lib/systemd/system-preset
install -s -m 755 am-i-scheduled $RPM_BUILD_ROOT/usr/sbin/am-i-scheduled
install -m 644 scheduling-monitor.service $RPM_BUILD_ROOT/lib/systemd/system/scheduling-monitor.service
install -m 644 scheduling-monitor.preset $RPM_BUILD_ROOT/lib/systemd/system-preset/50-scheduling-monitor.preset

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README.md

/usr/sbin/am-i-scheduled
/lib/systemd/system/scheduling-monitor.service
/lib/systemd/system-preset/50-scheduling-monitor.preset

%post
%systemd_post scheduling-monitor.service
systemctl start scheduling-monitor > /dev/null 2>&1

%preun
%systemd_preun scheduling-monitor.service

%postun
%systemd_postun_with_restart scheduling-monitor.service
