am-i-scheduled: am-i-scheduled.c
	cc -Wall -O2 am-i-scheduled.c -o am-i-scheduled

# On RHEL, it needs to be compiled like this
# cc -Wall am-i-scheduled.c -o am-i-scheduled -lrt

am-i-scheduled.src.rpm: am-i-scheduled.spec
	rpmbuild -ba am-i-scheduled.spec

am-i-scheduled-1.0.0.src.tar.gz: am-i-scheduled.c Makefile README.md scheduling-monitor.service scheduling-monitor.preset
	mkdir -p am-i-scheduled-1.0.0
	cp am-i-scheduled.c am-i-scheduled-1.0.0/
	cp Makefile am-i-scheduled-1.0.0/
	cp README.md am-i-scheduled-1.0.0/
	cp scheduling-monitor.service am-i-scheduled-1.0.0/
	cp scheduling-monitor.preset am-i-scheduled-1.0.0/
	tar cvfz am-i-scheduled-1.0.0.src.tar.gz am-i-scheduled-1.0.0
	rm -rf am-i-scheduled-1.0.0/

am-i-scheduled-1.0.0-1.x86_64.rpm: am-i-scheduled-1.0.0.src.tar.gz am-i-scheduled-1.0.0.spec
	mkdir -p ~/rpmbuild
	mkdir -p ~/rpmbuild/BUILD
	mkdir -p ~/rpmbuild/BUILDROOT
	mkdir -p ~/rpmbuild/RPMS
	mkdir -p ~/rpmbuild/SOURCES
	mkdir -p ~/rpmbuild/SPECS
	mkdir -p ~/rpmbuild/SRPMS
	cp am-i-scheduled-1.0.0.src.tar.gz ~/rpmbuild/SOURCES
	cp am-i-scheduled-1.0.0.spec ~/rpmbuild/SPECS
	rpmbuild -ba ~/rpmbuild/SPECS/am-i-scheduled-1.0.0.spec
	cp ~/rpmbuild/RPMS/x86_64/am-i-scheduled-1.0.0-1.x86_64.rpm ./
	cp ~/rpmbuild/SRPMS/am-i-scheduled-1.0.0-1.src.rpm ./

clean:
	rm -f am-i-scheduled-1.0.0.src.tar.gz
	rm -f am-i-scheduled-1.0.0-1.x86_64.rpm

# And sign it by making sure that .rpmmacros has
#   %_signature gpg
#   %_gpg_name  Greg Baker
# in it, and then run
# rpm --addsign am-i-scheduled-1.0.0-1.x86_64.rpm
